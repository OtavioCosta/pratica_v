public class Conta {
    
    private String id;
	private String tipo;
	private Cliente cliente;
	private double saldo;
	
	public void CadastrarConta(String id, String tipo, Cliente cliente, double saldo) {
		this.id = id;
		this.tipo = tipo;
		this.cliente = cliente;
		this.saldo = saldo;
	}	
	
	public void tipoConta() {
		if(this.tipo  == "1") {
			this.tipo = "Conta Poupança";
		}else if(this.tipo  == "2") {
			this.tipo = "Conta Corrente";
		}
			
	}
	
	public double sacarDinheiro(double valor) {
		if(this.saldo >= valor) {
			this.saldo -= valor;
			return valor;
		}
		return 0;
	}	
	
	public void depositarDinheiro(double valor) {
		this.saldo += valor;
	}	
	
	public void consultarConta() {
		System.out.println("-----------------------");
		System.out.println("Id: " + this.id);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("Saldo: " + this.saldo);
		System.out.println("Clinte: ");
		this.cliente.consultarCliente();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
}