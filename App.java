import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		
		List<Cliente> clientes = new ArrayList<>();
		List<Conta> contas = new ArrayList<>();
		
		Cliente cliente = new Cliente();
		cliente.CadastrarCliente("Maria", 46);
		
		clientes.add(cliente);
		
		Banco b = new Banco(clientes, contas);
		
		MenuBanco menu = new MenuBanco();
		menu.abrirMenu(b);

	}

}