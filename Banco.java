import java.util.List;
import java.util.Scanner;

public class Banco {
    
    Scanner scanner = new Scanner(System.in);
	private List<Cliente> listCliente;
	private List<Conta> listConta;
	
	public Banco(List<Cliente> listCliente, List<Conta> listConta) {
		this.listCliente = listCliente;
		this.listConta = listConta;
	}
	
	public void adicionarCliente(Cliente cliente) {
		this.listCliente.add(cliente);		
	}
	
	public void removerCliente(Cliente cliente) {
		this.listCliente.remove(cliente);
	}
	
	public void consultarClientes() {
		int i = 0;
		for (Cliente cliente : this.listCliente) 
            System.out.println(i++ + "- " + cliente.getNome());   
	}
	
	public Cliente buscarCliente() {
		int opcao;
		this.consultarClientes();
		System.out.println("-----------------------");
		System.out.print("Escolha o cliente: ");
		opcao = scanner.nextInt();
		if(opcao < this.listCliente.size() && opcao >= 0)		
			return this.listCliente.get(opcao);
		else return this.listCliente.get(0);
		
	}
	
	public void adicionarConta(Conta conta) {
		this.listConta.add(conta);		
	}
	
	public void removerConta(Conta conta) {
		this.listConta.remove(conta);
	}
	
	public void consultarConta() {
		int i = 0;
		for (Conta conta : this.listConta) 
            System.out.println(i++ + "- " + conta.getId());   
	}
	
	public Conta buscarConta() {
		int opcao;
		this.consultarConta();
		System.out.println("-----------------------");
		System.out.print("Escolha a conta: ");
		opcao = scanner.nextInt();
		if(opcao < this.listConta.size() && opcao >= 0)		
			return this.listConta.get(opcao);
		else return this.listConta.get(0);
		
	}

	public List<Conta> getListaContas() {
		return this.listConta;
	}

}
