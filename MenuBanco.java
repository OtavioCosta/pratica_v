import java.util.Scanner;

public class MenuBanco {

    Scanner scanner = new Scanner(System.in);
	
	public void abrirMenu(Banco b) { 
		int opcao;
		do {      
			System.out.println("-----------------------");
			System.out.println("1-Gerenciar CLIENTES");
			System.out.println("2-Gerenciar CONTAS");
			System.out.println("3-SAIR");
			System.out.println("-----------------------");
			
		    System.out.print("Escolha uma opção: ");
		    opcao = scanner.nextInt();
		    
		    switch(opcao) {
		    case 1:
		    	System.out.println("-----------------------");
		    	System.out.println("1-Gerenciar CLIENTES");
		    	System.out.println("-----------------------");
		    	System.out.println("1-Cadastrar CLIENTE");
				System.out.println("2-Consultar CLIENTE");
				System.out.println("3-Remover CLIENTE");
				System.out.println("4-Atualizar CLIENTE");
				System.out.println("5-Voltar ao MENU INICIAL");
				System.out.println("-----------------------");
				

				int opcaoClientes = 0;
				
				System.out.print("Escolha uma opção: ");
				opcaoClientes = scanner.nextInt();
				
				switch(opcaoClientes) {
			    case 1:
			    	System.out.println("-----------------------");
			    	System.out.println("1-Cadastrar CLIENTE");
			    	System.out.println("-----------------------");
			    	Cliente novoCliente = new Cliente();
			    	String nome;
			    	int idade;
			    	System.out.print("Nome: ");
			    	nome = scanner.next();
			    	System.out.print("Idade(int): ");
			    	idade = scanner.nextInt();
			    	novoCliente.CadastrarCliente(nome, idade);
			    	b.adicionarCliente(novoCliente);
			      break;
			    case 2:
			    	System.out.println("-----------------------");
			    	System.out.println("2-Consultar CLIENTE");
			    	System.out.println("-----------------------");
			   	  	b.buscarCliente().consultarCliente();
			   	
			      break;
			    case 3: 
			    	System.out.println("-----------------------");
			    	System.out.println("3-Remover CLIENTE");
			    	System.out.println("-----------------------");			    	 
			    	 b.removerCliente(b.buscarCliente());
			    	
			      break;
			    case 4: 
			    	System.out.println("-----------------------");
			    	System.out.println("4-Atualizar CLIENTE");
			    	System.out.println("-----------------------");
			    	b.buscarCliente().atualizarCliente();
		
			      break;
			    case 5: 
			      break;
			    default:
			    	System.out.println("Opção Inválida!");
			  }
								
		    	break;
			case 2:
				System.out.println("-----------------------");
		    	System.out.println("2-Gerenciar CONTAS");
		    	System.out.println("-----------------------");
		    	System.out.println("1-Criar CONTA para um CLIENTE");
				System.out.println("2-Sacar dinheiro de uma CONTA de um CLIENTE");
				System.out.println("3-Depositar dinheiro para uma CONTA de um CLIENTE");
				System.out.println("4-Verificar saldo de uma CONTA de um CLIENTE");
				System.out.println("5-Consultar contas");
				System.out.println("6-Voltar ao MENU INICIAL");
				System.out.println("-----------------------"); 
				
				int opcaoContas = 0;
				
				System.out.print("Escolha uma opção: ");
				opcaoContas = scanner.nextInt();
				
				switch(opcaoContas) {
			    case 1:
			    	System.out.println("-----------------------");
			    	System.out.println("1-Criar CONTA para um CLIENTE");
			    	System.out.println("-----------------------");
			    	Conta novaConta = new Conta();
			    	String id;
			    	String tipo = "";
					boolean cont = true;
					int opção;
			    	System.out.print("Id: ");
					id = scanner.next();
					while (cont) {
						System.out.print("Conta Poupança (1)  /  Conta Corrente (2): ");
						opcao = scanner.nextInt();
						if(opcao == 1) {
							tipo = "Conta Poupança";
							cont = false;
						} else if(opcao == 2) {
							tipo = "Conta Corrente";
							cont = false;
						} else {
							System.out.println("Opção Inválida");
						}
					}
			    
			    	Cliente cliente = b.buscarCliente();
			    	novaConta.CadastrarConta(id, tipo, cliente, 0);
			    	b.adicionarConta(novaConta);
			    	cliente.setConta(novaConta);
			    	
			      break;
			    case 2:
			    	System.out.println("-----------------------");
			    	System.out.println("2-Sacar dinheiro de uma CONTA de um CLIENTE");
					System.out.println("-----------------------");
					if(b.getListaContas().size() > 0) {
						System.out.println("Valor: ");
						System.out.println("-----------------------");
						double valor = scanner.nextDouble();					
						b.buscarCliente().getConta().sacarDinheiro(valor);
					} else System.out.println("Sem contas no sistema");
			   	    	
			   	
			      break;
			    case 3: 
			    	System.out.println("-----------------------");
			    	System.out.println("3-Depositar dinheiro para uma CONTA de um CLIENTE");
					System.out.println("-----------------------");
					if(b.getListaContas().size() > 0) {
						System.out.println("Valor: ");
						System.out.println("-----------------------");
						double valor2 = scanner.nextDouble();
						b.buscarCliente().getConta().depositarDinheiro(valor2);
					} else System.out.println("Sem contas no sistema");
			    	
			      break;
				case 4: 
					System.out.println("-----------------------");
					System.out.println("4-Verificar saldo de uma CONTA de um CLIENTE");
					System.out.println("-----------------------");
					if(b.getListaContas().size() > 0) {
						System.out.println("Saldo: " + b.buscarCliente().getConta().getSaldo());
					} else System.out.println("Sem contas no sistema");
			    	
				  break;
				case 5:
					System.out.println("-----------------------");
					System.out.println("5-Consultar contas");
					System.out.println("-----------------------");
					if(b.getListaContas().size() > 0) {
						b.buscarConta().consultarConta();
					} else System.out.println("Sem contas no sistema");
			    	break;
			    case 6:
			    	break;
			    default:
			    	System.out.println("Opção Inválida!");
			  }
				
				break;
			case 3: 
				System.out.println("Aplicação encerrada");
				break;		  
		    default:
		    	System.out.println("Opção Inválida!");
		  }
			
		} while (opcao != 3);
	}
}