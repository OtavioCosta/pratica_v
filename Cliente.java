import java.util.Scanner;

public class Cliente {
    
    Scanner scanner = new Scanner(System.in);
	private String nome;
	private int idade;
	private Conta conta;
	
	public void CadastrarCliente(String nome, int idade) {
		this.nome = nome;
		this.idade = idade;
	}	
	
	public void consultarCliente() {
		System.out.println("-----------------------");
		System.out.println("Nome: " + this.nome);
		System.out.println("idade: " + this.idade);
	}	
	
	public void atualizarCliente() {
		System.out.println("-----------------------");
		System.out.print("Novo nome: ");
		this.nome = scanner.next();
		System.out.print("Nova idade(int): ");
		this.idade = scanner.nextInt();
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

}